class Multiply {
	
	private static final double FIRST_OP = 2;	
	private static final double SECOND_OP = 35;
	
    public static void main(String[] args) throws Exception {
    	     	
        System.out.println("Result: " + (FIRST_OP * SECOND_OP));
        
    }
}
