package Lab3.Server_JSON;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

public class CodeCompileHandler implements RequestHandler {

	private static String result;

	/**
	 * Возвращает список обрабатываемых запросов
	 */
	public String[] handledRequests() {

		return new String[] { "process" };
	}

	/**
	 * Обработчик запроса "process"
	 */
	public JSONRPC2Response process(JSONRPC2Request req, MessageContext ctx) {

		if (req.getMethod().equals("process")) {

			Map<String, Object> content = req.getNamedParams();

			createJavaFileFromContent(content.get("code"));

			try {
				runProcess("javac -d target/classes Multiply.java");
				result = runProcess("java -classpath ./target/classes Multiply");
				System.out.println(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return new JSONRPC2Response(result, req.getID());

		} else {

			// Method name not supported

			return new JSONRPC2Response(JSONRPC2Error.METHOD_NOT_FOUND, req.getID());
		}
	}

	/**
	 * Функция сохранения принятого кода в java-файл
	 * 
	 * @param content
	 */
	private static void createJavaFileFromContent(Object content) {
		try (FileWriter writer = new FileWriter("./Multiply.java", false)) {
			writer.write((String) content);
			writer.flush();
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}

	}

	/**
	 * Функция вывода в консоль InputStream скомпилированной программы.
	 * 
	 * @param name
	 * @param ins
	 * @return
	 * @throws Exception
	 */
	private static String printLines(String name, InputStream ins) throws Exception {
		String res = "";
		String line = "";
		BufferedReader in = new BufferedReader(new InputStreamReader(ins));
		while ((line = in.readLine()) != null) {
			System.out.println(name + " " + line);
			if(line != null) res += line;
		}
		return res;
	}

	/**
	 * Функция испонения команд командной строки (тавтология конечно, но хз как
	 * по другому описать :D)
	 * 
	 * @param command
	 * @return
	 * @throws Exception
	 */
	private static String runProcess(String command) throws Exception {
		String res = null;
		Process pro = Runtime.getRuntime().exec(command);
		res = printLines(command + " stdout:", pro.getInputStream());
		printLines(command + " stderr:", pro.getErrorStream());
		pro.waitFor();
		System.out.println(command + " exitValue() " + pro.exitValue());
		return res;
	}
}
