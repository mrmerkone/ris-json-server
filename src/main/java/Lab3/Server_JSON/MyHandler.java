package Lab3.Server_JSON;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.*;
import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Parser;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.Dispatcher;

@SuppressWarnings("restriction")
public class MyHandler implements HttpHandler {

	private static Dispatcher dispatcher;
	private static JSONRPC2Parser parse;

	/**
	 * Обработчик http запросов
	 */
	public void handle(HttpExchange t) throws IOException {
		String response = "";
		try {
			InputStream is = t.getRequestBody();
			String content = inputStreamToString(is);
			if (content.length() == 0)
				response = "no request";
			else {
				JSONRPC2Request req = parse.parseJSONRPC2Request(content);
				JSONRPC2Response res = dispatcher.process(req, null);
				response = res.toJSONObject().toJSONString();
			}
		} catch (JSONRPC2ParseException e) {

		}

		t.sendResponseHeaders(200, response.length());
		OutputStream os = t.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}	

	/**
	 * Функция для получения тела http запроса
	 * 
	 * @param in
	 * @return
	 */
	public static String inputStreamToString(InputStream in) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line).append("\n");
			}
			in.close();
			return sb.toString();
		} catch (IOException e) {
			return "";
		}
	}
	
	public static void main(String[] args) throws Throwable {
		dispatcher =  new Dispatcher();

		// Регистрация обработчиков
		dispatcher.register(new CodeCompileHandler());
		parse = new JSONRPC2Parser();

		// Создание сервера
		HttpServer server;
		try {
			server = HttpServer.create(new InetSocketAddress(8080),0);
			server.createContext("/", new MyHandler());
			server.setExecutor(null); // creates a default executor
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}